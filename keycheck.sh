#!/bin/bash

# Copyright (C) 2003-2007 Joerg Jaspert <joerg@debian.org> and others
# This little script is free software; you can redistribute
# it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; version 2.
#
# On Debian systems, the complete text of the GNU General Public
# License can be found in /usr/share/common-licenses/GPL-2 file.
#
# This little (and maybe bad) script is used by me (and maybe others) to
# check keys from NM's.
#
# First it syncs local copies of the debian-keyring with keyring.d.o
# (the keyring package is too old) and then it downloads the
# key of the NM from a keyserver in the local nm.gpg file.
#
# After that it shows the key and all signatures made by existing Debian
# Developers.
#
# Finally, it checks to make sure that the key has encryption and
# signature capabilities, and will continue to have them one month
# into the future.
#
# ~/debian/keyring.debian.org/keyrings/ will be created if it doesn't exist.
#
# Usage:
# give it one option, the keyid.

# Always exit if there is an error
set -eu

# For the rsync of the debian keyrings and for the nm.gpg
DESTDIR="${DEBHOME:-"$HOME/debian"}/keyring.debian.org/keyrings"
# Which keyserver
KEYSERVER=${KEYSERVER:-"keyserver.ubuntu.com"}
# Don't try to access the network? (-n)
NONET=""
# Keep nm.gpg after check? (-k)
# nm.gpg holds all keys you checked with this script.
KEEP=""
# The options for the gpg call in this script.
# Contains only options used in ALL gpg calls.
GPGOPTS="-q --no-options --no-default-keyring --no-auto-check-trustdb --keyring $DESTDIR/nm.gpg --trust-model always"
# Try to ensure that we can use the output in an English report
if locale -a | grep -qiE '^en_.+utf8'
then
	LC_ALL=`locale -a | grep -iE '^en_.+utf8' | head -n 1`
else
	LC_ALL=C
fi
export LC_ALL
# Return value
EXIT=0

# Parse options

while getopts "kn" opt ; do
	case $opt in
		k) KEEP=1 ;; # keep nm.gpg
		n) NONET=1 ;; # no net
	esac
done
shift `expr $OPTIND - 1`

KEYID=$(echo $1 | sed -e 's/^0x//' -e 's/ //g')
if [ -z "$KEYID" ] ; then
	echo "Usage: $0 [-kn] <keyid>"
	echo "  -k  keep nm.gpg"
	echo "  -n  skip rsync"
	exit 1
fi

# Get keyrings and key

test -d $DESTDIR || mkdir -p $DESTDIR

if [ -z "$NONET" ] ; then
	rsync -qcltz --block-size=8192 --partial --progress \
		--exclude='emeritus-*' --exclude='removed-*' \
		'keyring.debian.org::keyrings/keyrings/*' $DESTDIR/.

	gpg ${GPGOPTS} --keyserver=$KEYSERVER --recv-keys "$KEYID"
else
	gpg --export -a "$KEYID" | gpg ${GPGOPTS} --import
fi

# Check key

gpg ${GPGOPTS} -v --with-fingerprint \
	--keyring $DESTDIR/debian-keyring.gpg \
	--keyring $DESTDIR/debian-nonupload.gpg \
	--check-sigs $KEYID

FPRLENGTH=$(gpg ${GPGOPTS} --with-colons --with-fingerprint --list-keys "$KEYID" |
	awk -F : '$1 == "fpr" {print length($10)}' | uniq)
if [ "$FPRLENGTH" -eq 32 ]; then
	echo "Warning: It looks like this key is an version 3 GPG key. This is bad."
	echo "This is not accepted for the NM ID Step. Please doublecheck and then"
	echo "get your applicant to send you a correct key if this is script isn't wrong."
	EXIT=1
else
	echo "Key is OpenPGP version 4 or greater."
fi

# pub:f:1024:17:C5AF774A58510B5A:2004-04-17:::-:Christoph Berg <cb@df7cb.de>::scESC:
KEYSIZE=$(gpg ${GPGOPTS} --with-colons --with-fingerprint --list-keys "$KEYID" |
	awk -F : '$1 == "pub" {print $3}')
if [ "$KEYSIZE" -ge 4096 ] ; then
	echo "Key has $KEYSIZE bits."
elif [ "$KEYSIZE" -ge 2048 ] ; then
	echo "Key has only $KEYSIZE bits.  Please explain why this key size is used"
	echo "(see [KM] for details)."
	echo "[KM] https://lists.debian.org/debian-devel-announce/2010/09/msg00003.html"
	EXIT=1
else
	echo "Key has only $KEYSIZE bits.  This is not acceptable."
	EXIT=1
fi

# pub:f:1024:17:C5AF774A58510B5A:2004-04-17:::-:Christoph Berg <cb@df7cb.de>::scESC:
KEYALGO=$(gpg ${GPGOPTS} --with-colons --with-fingerprint --list-keys "$KEYID" |
	awk -F : '$1 == "pub" {print $4}')
case $KEYALGO in
1)
	#echo "This is an RSA key."
	;;
17)
	echo "This is an DSA key.  This might need an explanation (see [KM] for details)."
	echo "[KM] https://lists.debian.org/debian-devel-announce/2010/09/msg00003.html"
	EXIT=1 ;;
*)
	echo "Unknown key algorithm $KEYALGO."
	EXIT=1 ;;
esac

# this awk script checks to see whether the key details on stdin show
# a valid usage flag for a given future date.
#    (author: Daniel Kahn Gillmor <dkg@fifthhorseman.net>)
#
# it needs two variables set before invocation:
#  KEYFLAG: which flag are we looking for?  (see https://tools.ietf.org/html/rfc4880#section-5.2.3.21
#    gpg supports at least:
#      a (authentication), e (encryption), s (signing), c (certification)
#  TARGDATE: unix timestamp of date that we care about
AWK_CHECKDATE='
 BEGIN {
  PRIFLAGS = "";
  SUBFOUND = 0;
 }
 $1 == "pub" && $2 != "r" {
  PRIFLAGS = $12;
  PRIEXP = $7;
  PRIFPR = $5;
 }
 $1 == "sub" && $2 != "r" && $12 ~ KEYFLAG {
  if (!SUBFOUND || $7 == "" || (SUBEXP != "" && $7 > SUBEXP) )
    SUBEXP = $7;
  SUBFOUND = 1;
 }
 END {
 if (PRIFLAGS ~ KEYFLAG)
  EXPIRES=PRIEXP;
 else if (!SUBFOUND)
  { print "No valid \"" KEYFLAG "\" usage flag set on key " PRIFPR "!" ; exit 1 }
 else if (PRIEXP != "" && PRIEXP < SUBEXP)
  EXPIRES=PRIEXP;
 else
  EXPIRES=SUBEXP;
 if ( "" == EXPIRES )
  print "Valid \"" KEYFLAG "\" flag, no expiration.";
 else if ( EXPIRES > TARGDATE )
  print "Valid \"" KEYFLAG "\" flag, expires " strftime("%c", EXPIRES) ".";
 else {
  print "Valid \"" KEYFLAG "\" flag, but it expires " strftime("%c", EXPIRES) ".";
  print "This is too soon!";
  print "Please ask the applicant to extend the lifetime of their OpenPGP key.";
  exit 1;
 }
 }
'

# we want to make sure that there will be usable, valid keys three months in the future:
EXPCUTOFF=$(( $(date +%s) + 86400*30*3 ))

gpg ${GPGOPTS} --with-colons --fixed-list-mode --list-key "$KEYID" | \
  gawk -F : -v KEYFLAG=e -v "TARGDATE=$EXPCUTOFF" "$AWK_CHECKDATE" || EXIT=$?
gpg ${GPGOPTS} --with-colons --fixed-list-mode --list-key "$KEYID" | \
  gawk -F : -v KEYFLAG=s -v "TARGDATE=$EXPCUTOFF" "$AWK_CHECKDATE" || EXIT=$?

# Clean up

if [ -z "$KEEP" ]; then
	rm -f $DESTDIR/nm.gpg $DESTDIR/nm.gpg~
fi

if [ "$EXIT" != 0 ] ; then
	cat <<EOF

#########################################################################
### There are problems with the key that might make it unusable for   ###
### inclusion in the Debian keyring or usage with Debian's LDAP       ###
### directory.  If unsure, please get in touch with the NM front-desk ###
### nm@debian.org to resolve these problems.                          ###
### (Please do not include this notice in the AM report.)             ###
#########################################################################
EOF
fi

exit $EXIT
