Here comes the first part of P&P, Philosophy and Procedures. The first
part is about philosophy - what makes free software and Debian free.

In preparation, you should read the Debian Constitution [1], the Social
Contract and the Free Software Guidelines [2], the Developer's Reference
[3], the Debian Code of Conduct [4], the Diversity Statement [5], and finally
the Debian Policy [6]. If any questions arise, you should have a look at the
DFSG FAQ draft [7]. (If you read any of these locally, be sure to fetch the
package from unstable!)

  [1] https://www.debian.org/devel/constitution
  [2] https://www.debian.org/social_contract
  [3] https://www.debian.org/doc/developers-reference
  [4] https://www.debian.org/code_of_conduct
  [5] https://www.debian.org/intro/diversity
  [6] https://www.debian.org/doc/debian-policy
  [7] https://people.debian.org/~bap/dfsg-faq.html

As we will exchange several mails, here are some notes to make things
easier:

* Please be verbose in your answers. If in doubt, write one sentence
  more. Especially in this first part, your written answers are the only
  way we can check your knowledge (no packages involved). It will save
  you some prodding replies from me. :)
* You can reply with URLs, but I would prefer if you could summarize the
  content in a few words.
* While we are discussing a question, please do not remove the number of
  the item from the quoted part so we can more easily see what we are
  talking about.
* If you like, reply in several mails. Postponing a message just because
  you have not yet had time to work on the other half of questions helps
  neither you nor me.
* Again, if you have questions: please ask them! I am glad to help you,
  and sometimes I can even learn something myself.

Here is the first set of questions:


Philosophy
----------

PH0. First, please have a careful read of the Social Contract and
     the DFSG. What do you think are their main points?

Secondly, a few questions, based on them:

PH1. What is Debian's approach to non-free software? Why? Is non-free
     part of the Debian System? Please also explain the difference
     between non-free and the other sections.

PH2. Suppose that Debian were offered a Debian-specific license to
     package a certain piece of software: would we put it in main?

PH3. Please explain the difference between free speech and free beer.
     Is Debian mainly about free speech or free beer?

PH4. What is your opinion about how the DFSG should be applied to files
     that are not software, such as documentation, firmware, images,
     etc? Specifically, DFSG section 2 "Source Code".

PH5. How do you check if a license is DFSG-compatible? Who has the final
     say over what can be included in Debian?

PH6. There are a few "tests" for this purpose, based on (not really)
     common situations. Explain them to me and point out which common
     problems can be discovered by them.

PH7. At https://people.debian.org/~joerg/bad.licenses.tar.bz2 you can
     find a tarball of bad licenses. Please compare the graphviz and
     three other (your choice) licenses with the first nine points of
     the DFSG and show a few examples of where they do not comply with
     the DFSG. There's no need to compare word for word (which would be
     impossible for some licenses anyway), but you should spot the
     biggest mistakes.
     Note: the graphviz license is bad for the brain: don't take too
     much of it.
     Also note: graphviz and qmail have now changed licenses in favour
     of free ones.

PHa. Are there any sections of the DFSG or Social Contract that you
     might like to see changed? If so, which ones, and why?

After you have mailed this back to me, I will go over your answers. If
all is satisfactory, I will send you part 2 of the P&P test.


Interesting URLs
----------------

Finally, some important web links:

For everybody:

  https://www.debian.org/devel/
             The Developer's Corner. Contains links and on-line versions
             of the stuff I mentioned before.

  https://www.debian.org/intro/organization
             Debian organizational structure

  https://www.debian.org/News/weekly/
             The Debian Project News, newsletter for the Debian
             community.

  https://wiki.debian.org
             The Debian wiki

  https://contributors.debian.org
             Info about Debian contributors. Some contributions are not
             tracked yet, this is work in progress.

  https://db.debian.org/
             Queries about developers and machines.

  https://salsa.debian.org/
             The Debian collaborative development server. It is a gitlab
             instance, providing code/file git repositories.

  https://qa.debian.org/
             The Debian Quality Assurance headquarters. Help is
             appreciated!

  https://bugs.debian.org/
             The bug tracking system (BTS).
             Note that there are some pseudo-packages related to the
             Debian infrastructure. For example
             https://bugs.debian.org/www.debian.org
             will list problems and issues related to the Debian
             website team.

  https://www.debian.org/Bugs/pseudo-packages
             The list of pseudo-packages

  https://www.debian.org/security/
             Security related info. Please read their FAQ, as it will
             save you (and others) a lot of headaches.

  https://packages.debian.org/
             Information about packages in the Debian archive.

  https://lists.debian.org/
             Mailing list subscription and archives.

  https://wiki.debian.org/Services
             Services for the Debian community, whether hosted in
             debian.org machines, or hosted in Developers/Contributors
             machines (and named .debian.net as part of the Debian
             community services).

  https://wiki.debian.org/WritingDebianPackageDescriptions
             A guide on writing package descriptions.
             It can be useful if you want to contribute reviews of
             those descriptions (e.g. in the debian-l10n-english team).

Particularly interesting for package uploading applicants:

  https://www.debian.org/devel/wnpp/
             The Work Needing and Prospective Packages list. What's
             orphaned, what needs new maintainers, what's being packaged
             and what would be nice to have packaged.

  https://buildd.debian.org/
             Build status of Debian packages.

  https://ci.debian.net/
             The Debian continuous integration (debci) is an automated
             system that coordinates the execution of automated tests
             against packages in the Debian system.

  https://qa.debian.org/developer.php
             Provides you with an overview of all your packages.

  https://lintian.debian.org/
             Automated lintian tests on all packages in the Debian
             Archive.

  https://tracker.debian.org/
             The Debian Package Tracker.

  https://ftp-master.debian.org/REJECT-FAQ.html
             Frequently asked questions about why a package is rejected
             from the NEW queue.
