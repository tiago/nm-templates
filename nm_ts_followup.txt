This file contains a list of common followup questions or information commonly
asked or provided after answers to nm_ts questions.


* PF1. What does version 2:3.4~rc1-2.1+lenny1 mean?

Can you think of a reason why it is "3.4~rc1" instead of "3.4.rc1"?
Can you infer something from seeing a dot in the Debian revision?


* PF2. What does the version string in the Standards-Version field of a
       package's control file represent? Why is it useful?

Suppose you have a package with an old Standards-Version field.  How do
you bring the package up to speed with the most recent policy without
needing to find an old version of the policy and wdiff it against the
new one?


* PF7. What is Essential: yes? Why isn't libc essential and why can't it
       be? Why does it not need to be essential? Why isn't the kernel
       essential?

Yes, and also you can build your own kernel without making it a Debian
package, or in fact a Debian system can run perfectly well without a
kernel installed at all, like in the case of a chroot.


* PFb. There is a minimal set of packages you never need to Build-Depend
       on. How do you find which ones? What is the reason for it?

Yes, and also you want to be able to do changes in the basic toolchain
without needing to reupload half of the archive with new
build-dependencies.

The same technique is for example adopted by the Java maintainers
without using build-essential but by providing a default-jdk metapackage
that people can build-depend on.


* PP2. How can you ensure your package's description is in a good state and
       in a valid format?

See the policy Section 3.4, `The description of a package' for further
information on this.

