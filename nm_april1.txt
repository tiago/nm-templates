Here is an extra round of questions I would like you to answer:

XR1. Please tell me 3 arrogant methods to close a bug in the BTS, the
     difference between them, and when to use which method.

XR2. Have you ever taken part in a flamewar? If not, please choose a recent
     thread in debian-devel@lists.debian.org and try to be the last person
     posting to it.

XR3. Please list at least 3 Easter eggs that can be found in software in Debian
     main.

XR4. How is Debian better than Ubuntu? If you have never used Ubuntu, how is
     Debian better than Fedora? Or RHEL? Or Gentoo? Please also post the reply
     in a relevant online forum.

XR5. The Packages file contains information copied from the control files of
     several packages, each having its own different license. What is the
     license of the Packages file? Can it be distributed at all?

XR6. Is there a list of Debian Developers whose messages you always skip when
     reading mail? How long is it? Am I in it? Would you like to have
     suggestions for extra names to add to it?

XR7. An American DD, a German DD and an Italian DD are comaintaining a package.
     Which one is likely to introduce the largest number of bugs?

XR8. When a DD writes English documentation for his packages, how can he make
     sure that he is using gender neutral language?

XR9  Please visit http://bugs.debian.org/cgi-bin/pkgreport.cgi?which=maint&data=enrico%40debian.org&archive=no&raw=yes&bug-rev=yes&pend-exc=fixed&pend-exc=done
     and fix all the bugs listed there.

XR10. What does version 1:9.8.4.dfsg.P1-6+nmu2+deb7u1~bpo60+1 mean? Please try
      to find an even weirder version number that we can use to startle new
      developers.

XR11. Until woody we have had sex without love, and since squeeze we have love
      without sex. What happened between the two releases? What is going to
      happen to Debian in the future?

XR12. Please explain the difference between free speech, free beer, speech-free
      and beer-free.

XR13. If you think these jokes are old because you have already received them
      last year, please explain what have you been doing for a year in NM.
