
Next I have some questions and tasks for you. This will form one part of
your T&S Check. The other check is a summary, which I will request from the
people who are working with you. If you haven't already, then please
tell me their names and email addresses. (This means people working
with you on Debian and documentation related stuff, not people at your
paid job).


First we have to check that you already doing work as a documentation
maintainer. There are different ways to show me this:

 a. Tell me your cvs account name you have for the web and DDP
    work. Then I can check your work there.
 b. If you don't have a cvs account at cvs.debian.org then please point
    me to the messages in the mailinglist archive with your patches to
    the documentation/web stuff.
 c. You could also point me to bugreports where you sent documentation related
    patches to different packages.

If you are not able to answer at least one point of this then I will
put you on hold for a while to give you the chance to work on that
before we go on with your application, because there is already
a good framework for non-DD to work on documentation related things.

Also, if you have filed bug reports (with patches) to the BTS with
respect to documentation, please tell me the bug numbers, so I can
check them.


And now, some quick questions:

  0. What is the best way to check that your Build-Depends Line contains
     everything required to build your package?

  1. Please explain to me what a virtual package is. Where can you find a
     list of defined virtual packages?

  2. What is the difference between "Architecture: all" and
     "Architecture: any"?

  3. What is the difference between native packages and non-native
     packages?

  4. What is an epoch? When would you use one?

  5. Explain the difference between Depends, Recommends, and Suggests.

  6. Tell me the differences between /usr/share and /usr/lib.

  7. Please list some good reasons for a package split.

  8. What does the "urgency" field in changelog affect?

  9. What are maintainer scripts? What arguments does each get?
     What is the meaning of the arguments? What can each of them
     assume about the system?

 10. What is the difference between experimental and unstable?

 11. What would you do if you had a package which includes a mix of
     architecture-dependent and non-architecture-dependent files?

 12. There is a minimal set of packages you never need to Build-Depend
     on. Tell me which and why.

 13. What build target would you use in debian/rules to build a package
     which includes only non-architecture-dependent files? What is the
     "Architecture:" field for this package?

 14. Explain within 5 lines what "locale" technology means to you and
     point us to the best documentation on the subject for Debian.

 15. What default locale do you set before building a documentation
     package?

 16. Does ASCII code include so called high-bit characters?

 17. What is ISO-8859-15? UTF-8? Unicode?


After these basic packaging related questions comes some documentation
related stuff. (The above are true basics, so you should really know this.)

Next we will check your knowledge of SGML and all related
tools/concepts. This includes pure SGML, but also DocBook, Stylesheets,
document processors that apply the sheets to SGML documents, and the
scripting (or makefile) glue needed to automate this.

 - Please choose one Debian subsystem, and document it thoroughly.
#  Please document the Debian subsystem XY thoroughly.                #
   You can choose one of three target audiences for your work: novice
   user, advanced user, or Debian developer. Be warned that the
   documentation for novice users must actually teach them the chosen
   topic _in depth_.

   The document should be written using the SGML or XML DocBook DTD. It
   should be transformed (using the toolset of your choice among those in
   Debian stable) into: high quality PDF, high quality PostScript,
   W3C-compliant HTML document (in two variants: as a single large file,
   and as smaller hyperlinked files), and properly formatted plain text.

   The document must include at least one table, at least one image or
   diagram, a TOC, an index, and one item of out-of-band information
   (footnote or margin note). You are expected to make proper use of
   these: they must fit well within the whole document.

   You should use the DDP for this work, so I can check your skills in
   usage of CVS. After you started your work, tell me the commands I
   need to get your working directory from the CVS on my system. (Using
   CVS over SSH of course).


Now we have checked that you are able to write good documentation. But
that's not all, we have to check if you are able to summarize technical
documents, so you will have to write two manual pages.

 - The first manpage should describe ##insert here a section of
   debian-policy or another extended document##. It must be placed at the
   proper section and subsection (if any) for manpages, and it must have
   proper apropos(1) and whatis(1) behaviour when installed into the
   system.

   The second manpage should be for a binary without a manpage in the
   Debian System. You can freely choose which tool you want to
   document. If the manpage is rather short, you should write manpages
   for two or more utilities. For a list of binaries without manpages
   you can look at the lintian site [1].
   This manpage(s) then should be submitted to the maintainer of the
   package using the BTS, with a properly filed bug report.

   You can use any of the available toolsets (bonus points if you can do
   it in SGML and generate a HTML version of the manpage as well) in
   Debian stable.


After all this, we still have something to check. First there is the
usage of GnuPG. As I request that all of your e-mail messages to me
be signed with GnuPG, you must have at least a basic knowledge of GnuPG
(or a good mailreader where it works out of the box). To show me a bit
more of your knowledge I ask you to do the following things:

 g1. Take a (small) binary file and create a gpg signature for it.
     Send me both.

 g2. Do the same thing, this time for an ASCII file, clearsigned.

 g3. Encrypt the ASCII file to my personal key.

 g4. I will send you an encrypted file with a short message in. Please
     decrypt that and send me the message (signed and encrypted mail).


The next check is about SSH. There is no way to live without it today, so
you really should read the documentation that comes with it if you are
not familiar with SSH. Here are some questions for you to answer about
SSH.

 s1. Discuss the advantages and disadvantages of SSH public key
     authentication compared with the "traditional" password
     authentication.

 s2. What are some advantages of SSH, compared to other remote login
     procedures?

 s3. How do you generate a ssh key pair?

 s4. Generate a key pair and send me the public key of it and tell me
     where to put it on my system so you are able to login.
     I will send you a hostname then. You have to connect there and
     create a few files.

 s5. Should you use things like GnuPG over a SSH connection? Why?


The last (for now) check is about the BTS. You have to know how to use it.
This is easy for you: If you submitted the manpages correctly using the
BTS and answered the BTS questions in the P&P part well, then you are
done with this. If not, be prepared for some more questions in one of my
next mails.


[List of URLs]
[1] https://lintian.debian.org/reports/Tbinary-without-manpage.html
